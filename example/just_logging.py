import logging
import time
for i in range(10):
    logger = logging.getLogger("GUILog Test - %s" % i)
    logger.debug("Debug")
    logger.info("Info")
    logger.warning("Warning")
    logger.error("Error")
    logger.critical("Critical")
    logger.log(42, "Not set")
time.sleep(1)